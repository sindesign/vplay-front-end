<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Course Details</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body class="pg-course-details">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
				<div class="summary-banner">
					<picture class="background" role="presentation">
						<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/max.jpg">
						<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/x-lrg.jpg">
						<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/lrg.jpg">
						<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/med.jpg">
						<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/sml.jpg">
						<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/min.jpg">
						<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/max.jpg" alt="">
					</picture>
					<img class="logo" src="img/brand/agrifoods-logo.png" alt="AgriFoods Logo">
					<p class="course-collection">
						Digital Strategy 101
					</p>
					<h1 class="title">
						Developing A Digital Strategy
					</h1>
				</div>

				<div class="video-and-summary">
					<div class="preview-video-container fluid-video">
						<div class="wrapper">
							<iframe src="//www.youtube.com/embed/Bo_f8mV5khg?rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="course-summary">
						<h2 class="title">
							About This Course
						</h2>
						<dl class="summary-list">
							<dt class="key">
								Course Instructor:
							</dt>
							<dd class="value instructor-name">
								Tim Gentle
							</dd>
							<dt class="key">
								Course Difficulty:
							</dt>
							<dd class="value">
								Beginner
							</dd>
							<dt class="key">
								Total Length:
							</dt>
							<dd class="value">
								3h 52m
							</dd>
							<dt class="key">
								Course Units:
							</dt>
							<dd class="value">
								12 Modules
							</dd>
							<dt class="key">
								Certification:
							</dt>
							<dd class="value">
								Certificate of Completion
							</dd>
							<dt class="key">
								Enrolments:
							</dt>
							<dd class="value">
								62 Students
							</dd>
						</dl>
						<p class="price">
							Free Course
						</p>
						<a href="" class="button filled-sc start-button">
							Start Now
						</a>
					</div>
				</div>

				<div class="course-description">
					<p>
						Digital strategy 101 is accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt ipraesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non pro.
					</p>
				</div>

				<div class="share-options">
					<h2 class="title">
						Share this course
					</h2>
					<ul class="social-links">
						<li>
							<a href="" class="social-link">
								<span class="visuallyhidden">Share via Facebook</span>
								<span class="socicon socicon-facebook"></span>
							</a>
						</li>
						<li>
							<a href="" class="social-link">
								<span class="visuallyhidden">Share via Twitter</span>
								<span class="socicon socicon-twitter"></span>
							</a>
						</li>
						<li>
							<a href="" class="social-link">
								<span class="visuallyhidden">Share via Google+</span>
								<span class="socicon socicon-google"></span>
							</a>
						</li>
						<li>
							<a href="" class="social-link">
								<span class="visuallyhidden">Share via LinkedIn</span>
								<span class="socicon socicon-linkedin"></span>
							</a>
						</li>
						<li>
							<a href="" class="social-link">
								<span class="visuallyhidden">Share via YouTube</span>
								<span class="socicon socicon-youtube"></span>
							</a>
						</li>
					</ul>
				</div>
				
				<div class="spacing-container">
					<div class="modules-overview">
						<h2 class="title">
							Course Modules
						</h2>
						<ul class="modules-list">
							<li>
								<span class="chapter-name">
									1. Introduction to long text line lengths
								</span>
								<span class="chapter-length">
									06:11
								</span>
							</li>
							<li>
								<span class="chapter-name">
									2. Introduction
								</span>
								<span class="chapter-length">
									06:11
								</span>
							</li>
							<li>
								<span class="chapter-name">
									3. Introduction
								</span>
								<span class="chapter-length">
									06:11
								</span>
							</li>
							<li>
								<span class="chapter-name">
									4. Introduction
								</span>
								<span class="chapter-length">
									06:11
								</span>
							</li>
							<li>
								<span class="chapter-name">
									5. Introduction
								</span>
								<span class="chapter-length">
									06:11
								</span>
							</li>
							<li>
								<span class="chapter-name">
									6. Introduction
								</span>
								<span class="chapter-length">
									06:11
								</span>
							</li>
						</ul>
					</div>
				</div>

				<h2 class="more-courses-title">
					More Think Digital Courses
				</h2>

				<div class="course-summary-list">
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
				</div> <?php /* course summary list */ ?>

			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>