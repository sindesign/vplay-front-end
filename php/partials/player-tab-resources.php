<h3>
	DOWNLOADS
</h3>

<a href="" class="button pc">
	Download this video
</a>

<ul class="downloads-list">
	<li>
		<a href="" class="blend-in download-item">
			Course_Overview.pdf
		</a>
	</li>
	<li>
		<a href="" class="blend-in download-item">
			Supplementary_Content.zip
		</a>
	</li>
	<li>
		<a href="" class="blend-in download-item">
			References.docx
		</a>
	</li>
</ul>