<?php
	
	define(BP_MIN, '320px');
	define(BP_SML, '480px');
	define(BP_MED, '640px');
	define(BP_LRG, '768px');
	define(BP_X_LRG, '1000px');
	define(BP_MAX, '1280px');

	define(BRAND_IMAGES_FOLDER, 'img/brand');
?>