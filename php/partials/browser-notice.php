<div role="alert" id="browser-upgrade-notice">
	<p class="message-content">
		You are using an outdated and unsupported browser. 
	</p>
	<p>
		To access this website, please use a browser such as <a href="http://firefox.com" target="_blank">Mozilla Firefox</a> or <a href="https://www.google.com/chrome/browser/" target="_blank">Google Chrome</a>.
	</p>
</div>