<img class="avatar" src="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/instructors/tim-gentle.jpg" alt="{{INSTRUCTOR NAME}}">
<figcaption class="details">
	<h3 class="name">
		Tim Gentle
	</h3>
	<div class="bio">
		<p>
			Tim Gentle is a courageous digital strategist and entrepreneur with a passion for building business and regional lifestyle. With over 10 years’ experience in the digital world and 10 years in Marketing.
		</p>
		<p>
			Tim’s expertise is his ability to help small business harness their online potential. Tim is a sought after thought leader, presenting nationally and internationally, delivering practical online strategy and solutions to audiences that are not only current and cutting edge, but easy to understand.
		</p>
	</div>
	<a class="reviews-link" href="">
		32 Instructor Reviews
	</a>
	<ul class="social-links">
		<li>
			<a href="" class="social-link">
				<span class="visuallyhidden">Tim Gentle on Facebook</span>
				<span class="socicon socicon-facebook"></span>
			</a>
		</li>
		<li>
			<a href="" class="social-link">
				<span class="visuallyhidden">Tim Gentle on Twitter</span>
				<span class="socicon socicon-twitter"></span>
			</a>
		</li>
		<li>
			<a href="" class="social-link">
				<span class="visuallyhidden">Tim Gentle on Google+</span>
				<span class="socicon socicon-google"></span>
			</a>
		</li>
		<li>
			<a href="" class="social-link">
				<span class="visuallyhidden">Tim Gentle on LinkedIn</span>
				<span class="socicon socicon-linkedin"></span>
			</a>
		</li>
		<li>
			<a href="" class="social-link">
				<span class="visuallyhidden">Tim Gentle on YouTube</span>
				<span class="socicon socicon-youtube"></span>
			</a>
		</li>
	</ul>
</figcaption>