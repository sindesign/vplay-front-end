<img class="thumbnail" src="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-thumbnails/course-thumb-03.jpg" alt="{{COURSE NAME}} Thumbnail">
<dl class="course-summary">
	<dt class="key visuallyhidden">
		Collection
	</dt>
	<dd class="value collection">
		Digital Strategy 101
	</dd>
	<dt class="key visuallyhidden">
		Course
	</dt>
	<dd class="value title">
		Developing a digital strategy <?php echo rand(0,1) ? '' : "for text over two lines bingle bongle doop"; ?>
	</dd>
	<dt class="key visuallyhidden">
		Instructor
	</dt>
	<dd class="value instructor">
		Tim Gentle
	</dd>
	<dt class="key visuallyhidden">
		Channel
	</dt>
	<dd class="value channel">
		Agrifoods Australia
	</dd>
		<?php echo rand(0,1) ? 
		'<dt class="key progress-key">
			Progress 70%
		</dt>
		<dd class="value progress">
			<div class="progress-bar progress-70"></div>
		</dd>'
	:
		'<dt class="key progress-key invisible">
			Progress
		</dt>
		<dd class="value progress">
			<span class="complete-notice">Course complete</span>
		</dd>';
	?>
	</dd>

	
</dl>

<h3 class="share-title">
	Share this course
</h3>
<ul class="social-links">
	<li>
		<a href="" class="social-link">
			<span class="visuallyhidden">Tim Gentle on Facebook</span>
			<span class="socicon socicon-facebook"></span>
		</a>
	</li>
	<li>
		<a href="" class="social-link">
			<span class="visuallyhidden">Tim Gentle on Twitter</span>
			<span class="socicon socicon-twitter"></span>
		</a>
	</li>
	<li>
		<a href="" class="social-link">
			<span class="visuallyhidden">Tim Gentle on Google+</span>
			<span class="socicon socicon-google"></span>
		</a>
	</li>
	<li>
		<a href="" class="social-link">
			<span class="visuallyhidden">Tim Gentle on LinkedIn</span>
			<span class="socicon socicon-linkedin"></span>
		</a>
	</li>
</ul>

<div class="button-holders">
	<a href="" class="button filled-pc">
		Continue course
	</a>
</div>