<h3>
	NOTES
</h3>

<form id="note-submit-form" action="javascript:void(0);">
	<input name="note" type="text" placeholder="Type notes here. Press enter to save.">
</form>
<div id="notes-container"></div>
<a href="javascript:app.downloadNotes();" class="button pc">
	Download Notes
</a>

<script>
	var app = app || {};

	app.notes = [
		{
			text: 'Sample note 1.'
		},
		{
			text: 'Some more sample text for a longer note. Perhaps long enough to span over multiple lines.'
		},
		{
			text: 'Just quickly, another note. Not as long as the last one.'
		},
		{
			text: 'With comments, anything goes!'
		},
		{
			text: 'Purple monkey dishwasher. When do we want it? Now!'
		}
	];

</script>