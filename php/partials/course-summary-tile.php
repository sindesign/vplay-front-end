<img class="thumbnail" src="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-thumbnails/course-thumb-03.jpg" alt="{{COURSE NAME}} Thumbnail">
<aside class="course-collection">
	Digital Strategy 101
</aside>
<h2 class="name">
	Developing a digital strategy <?php echo rand(0,1) ? '' : "for text over two lines bingle bongle doop"; ?>
</h2>
<div class="description">
	<p>
		At vero eos et accusamus et iusto odio<?php echo rand(0,1) ? '' : " dignissimos ducimus qui blanditiis praesentium voluptatum deleniti"; ?>.
	</p>
</div>
<dl class="course-summary">
	<dt class="key">
		Instructor
	</dt>
	<dd class="value instructor-name">
		Tim Gentle
	</dd>
	<dt class="key">
		Length
	</dt>
	<dd class="value">
		<?php echo rand(2,20); ?> Hrs
	</dd>
</dl>
<dl class="extra-information">
	<dt class="key">
		Price
	</dt>
	<dd class="value price-container">
		Free
	</dd>
	<dt class="key">
		View Course
	</dt>
	<dd class="value">
		<a href="" class="button filled-pc view-course-button">
			View Course
		</a>
	</dd>
</dl>
