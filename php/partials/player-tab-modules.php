<h3>
	MODULES
</h3>

<ul class="modules-list">
	<li>
		<a href="" class="blend-in completed">
			<h4 class="module-number">
				Module 1 (Completed)
			</h4>
			<p class="module-name">
				Introduction
			</p>
		</a>
	</li>
	<li>
		<a href="" class="blend-in">
			<h4 class="module-number">
				Module 2
			</h4>
			<p class="module-name">
				How to know when to hold 'em
			</p>
		</a>
	</li>
	<li>
		<a href="" class="blend-in current">
			<h4 class="module-number">
				Module 3
			</h4>
			<p class="module-name">
				How to know when to fold 'em
			</p>
		</a>
	</li>
	<li>
		<a href="" class="blend-in completed">
			<h4 class="module-number">
				Module 4 (Completed)
			</h4>
			<p class="module-name">
				Know when to walk away
			</p>
		</a>
	</li>
	<li>
		<a href="" class="blend-in">
			<h4 class="module-number">
				Module 5
			</h4>
			<p class="module-name">
				Know when to run
			</p>
		</a>
	</li>
</ul>