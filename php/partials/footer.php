<footer>
	<div class="clearfix">
		<div class="footer-section left">
			<ul class="social-links">
				<li>
					<a href="" class="social-link">
						<span class="visuallyhidden">Tim Gentle on Facebook</span>
						<span class="socicon socicon-facebook"></span>
					</a>
				</li>
				<li>
					<a href="" class="social-link">
						<span class="visuallyhidden">Tim Gentle on Twitter</span>
						<span class="socicon socicon-twitter"></span>
					</a>
				</li>
				<li>
					<a href="" class="social-link">
						<span class="visuallyhidden">Tim Gentle on Google+</span>
						<span class="socicon socicon-google"></span>
					</a>
				</li>
				<li>
					<a href="" class="social-link">
						<span class="visuallyhidden">Tim Gentle on LinkedIn</span>
						<span class="socicon socicon-linkedin"></span>
					</a>
				</li>
				<li>
					<a href="" class="social-link">
						<span class="visuallyhidden">Tim Gentle on YouTube</span>
						<span class="socicon socicon-youtube"></span>
					</a>
				</li>
			</ul>

			<p>
				<a href="">
					thinkdigital.education/channelname.com
				</a>
			</p>
			<p class="aux-links">
				<a href="">
					Home
				</a>
				<a href="">
					Learn
				</a>
				<a href="">
					Teach
				</a>
				<a href="">
					News
				</a>
				<a href="">
					Support
				</a>
				<a href="">
					About
				</a>
				<a href="">
					Contact
				</a>
			</p>
		</div>
		<div class="footer-section right">
			<a href="" class="button">
				TEACH ON THINK DIGITAL
			</a>
			<br>
			<a href="" class="button">
				REFER A FRIEND
			</a>
		</div>
	</div>

	<div class="clearfix">
		<div class="footer-section left topless">
			<p>
				We accept VISA / MasterCard / PayPal
			</p>
		</div>
		<div class="footer-section right topless">
			<p>
				<a href="">
					Privacy Policy
				</a>
				<a href="">
					Terms Of Use
				</a>
				
				<span>
					&copy; 2014 Design Experts
				</span>
			</p>
		</div>
	</div>
</footer>