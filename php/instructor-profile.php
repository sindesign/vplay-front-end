<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Intstructor Profile</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body class="pg-instructor-profile">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">

				<div class="certificate-section">
					<picture class="background" role="presentation">
						<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif">
						<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/x-lrg.gif">
						<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/lrg.gif">
						<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/med.gif">
						<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/sml.gif">
						<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/min.gif">
						<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif" alt="">
					</picture>
					<h1 class="status-info">
						Instructor Profile
					</h1>
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/tim-gentle.jpg" alt="Tim Gentle" class="avatar">
					<p class="name">
						Tim Gentle
					</p>
					<ul class="summary-icons">
						<li>
							<img src="img/icon_location.gif" alt="" role="presentation">
							Australia
						</li>
						<li>
							<img src="img/icon_brand.gif" alt="" role="presentation">
							Think Digital
						</li>
						<li>
							<img src="img/icon_courses.gif" alt="" role="presentation">
							10 Courses
						</li>
					</ul>
					<p class="">
						Tim Gentle is a courageous digital strategist and entrepreneur with a passion for building business and regional lifestyle. With over 10 years’ experience in the digital world and 10 years in Marketing.
					</p>
					<p>
						Tim’s expertise is his ability to help small business harness their online potential. Tim is a sought after thought leader, presenting nationally and internationally, delivering practical online strategy and solutions to audiences that are not only current and cutting edge, but easy to understand.
					</p>

					<div class="share-options">
						<h2 class="title">
							Connect with Tim Gentle
						</h2>
						<ul class="social-links">
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via Facebook</span>
									<span class="socicon socicon-facebook"></span>
								</a>
							</li>
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via Twitter</span>
									<span class="socicon socicon-twitter"></span>
								</a>
							</li>
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via Google+</span>
									<span class="socicon socicon-google"></span>
								</a>
							</li>
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via LinkedIn</span>
									<span class="socicon socicon-linkedin"></span>
								</a>
							</li>
						</ul>
					</div>

				</div>

				<h2 class="brand-color-title">
					More Courses By Tim Gentle
				</h2>

				<div class="course-summary-list">
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
				</div>

				<p class="view-courses-container">
					<a href="" class="button filled-pc">
						View all courses
					</a>
				</p>
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>