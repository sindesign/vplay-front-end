<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Think Digital Home</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body class="pg-brand-home">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
				<div class="hero-banner">
					<a href="">
						<picture>
							<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/max.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/max@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/x-lrg.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/x-lrg@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/lrg.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/lrg@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/med.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/med@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/sml.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/sml@2x.jpg 2x">
							<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/min.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/min@2x.jpg 2x">
							<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/max.jpg" alt="">
						</picture>
					</a>
					<h1 class="visuallyhidden">
						Think Digital
					</h1>
				</div>

				<h2 class="top-courses-title">
					Popular Think Digital Courses
				</h2>

				<div class="course-summary-list">
					<article class="course-summary-tile wide">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile wide">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile wide">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
				</div> <?php /* course summary list */ ?>

				<p style="text-align: center;">
					<a href="" class="button filled-pc">
						VIEW ALL COURSES
					</a>
				</p>

				<h2 class="top-courses-title">
					My Courses
				</h2>

				<div class="course-progress-list">
					<article class="course-progress-tile">
						<?php include("partials/course-progress-tile.php"); ?>
					</article>
					<article class="course-progress-tile">
						<?php include("partials/course-progress-tile.php"); ?>
					</article>
					<article class="course-progress-tile">
						<?php include("partials/course-progress-tile.php"); ?>
					</article>
				</div> <?php /* course progress list */ ?>

				<h2 class="top-courses-title">
					My Wishlist
				</h2>

				<div class="course-summary-list">
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
				</div>
		
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>