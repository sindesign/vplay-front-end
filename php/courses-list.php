<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Courses</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body class="pg-courses-list">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
				<div class="hero-section">
					<picture class="background" role="presentation">
						<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/max.jpg">
						<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/x-lrg.jpg">
						<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/lrg.jpg">
						<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/med.jpg">
						<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/sml.jpg">
						<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/min.jpg">
						<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/max.jpg" alt="">
					</picture>
					<img class="logo" src="img/brand/agrifoods-logo.png" alt="AgriFoods Logo">
					<h1 class="title">
						Digital Strategy 101
					</h1>
					<p class="description">
						AgriFood Skills Australia is the key body on skills and workforce development for the Australian agrifood industry and regional Australia.
					</p>
				</div>
				<div class="course-summary-list">
					<article class="course-summary-tile wide">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile wide">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
				</div> <?php /* course summary list */ ?>
				<section class="course-instructors-list">
					<h2 class="title">
						Course Instructors
					</h2>
					<figure class="instructor-spotlight">
						<?php include("partials/instructor-spotlight.php"); ?>
					</figure>
					<figure class="instructor-spotlight">
						<?php include("partials/instructor-spotlight.php"); ?>
					</figure>
				</section>
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>