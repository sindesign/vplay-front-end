<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Intstructor Profile</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body id="general-content-page" class="pg-instructor-profile">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
            	
                <picture class="background" role="presentation">
                    <img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif" alt="">
                    
                    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" valign="middle">
                        	<h1 class="status-info small-page-heading">
                                Terms &amp; Conditions
                            </h1>
                        </td>
                      </tr>
                    </table>
                </picture>

				<div class="certificate-section">
                    
                    <h2>This platform is operated by Preveu Limited ACN 154 009 686 (Preveu).</h2>
                    
                    <p>In these terms and conditions, the expressions we, us and our, are a reference to Preveu.If you use this platform, you are agreeing to be bound by the terms and conditions listed below and any other laws or regulations which apply to this platform.  If you do not accept these terms and conditions, you must refrain from using this platform. We reserve the right to amend these terms and conditions from time to time.  Amendments will be effective immediately upon notification on this platform.  Your continued use of this platform following such notification will represent an agreement by you to be bound by the terms and conditions as amended.</p>
                    
                    <h3>Intellectual property rights statement</h3>
                    
                    <p>All intellectual property rights in this platform, including design, text, graphics, video, logos, icons, sound recordings and all software relating to this platform belong to or are licensed by us.  These intellectual property rights are protected by Australian and international laws.</p>
                    
                    <p>You may not in any form or by any means copy, adapt, reproduce (other than for the purpose of viewing the platform in your browser), store, modify, distribute, print, upload, display, perform, remove any credits, publish post frame within another website or application, or create derivative works from any part of this platform or commercialise any information obtained from any part of this platform without our prior written permission or, in the case of third party material, from the owner of the intellectual property rights in that material.</p>
                    
                    <p>If you enter into a transaction for the use of digital content over this platform, you must agree to the separate terms and conditions applicable to that transaction and digital content before viewing or otherwise using the relevant digital content, including all applicable licensing restrictions and terms of use.  If you do not accept such terms and conditions, you must refrain from viewing or otherwise using the relevant digital content.</p>
                    
                    <h3>Linked websites and applications</h3>
                    
                    <p>This platform may contain links to third party websites and applications.  The links are provided for convenience only and may not remain current or be maintained.  We are not responsible for the content or privacy practices associated with linked websites and applications.</p>
                    
                    <h3>Secure data</h3>
                    
                    <p>Unfortunately, no data transmission over the internet can be guaranteed as totally secure.  We do not warrant and cannot ensure the security of any information which you transmit to us.  Accordingly, any information that you transmit to this platform is transmitted at your own risk.  If you become aware of any problems with the security of the data or the platform, please contact us immediately.</p>
                    
                    <h3>Warnings</h3>
                    
                    <p>You must ensure that your access to this platform is not illegal or prohibited by laws which apply to you.We do not warrant the accuracy, adequacy or completeness of the information, nor do we undertake to keep this platform updated.  The information on this platform is not, and is not intended to be, advice.  You should not act or refrain to act on the basis of any of the material on this platform without first satisfying yourself as to the truth or accuracy of all information given.</p>
                    
                    <p>We do not accept responsibility for loss suffered as a result of reliance by you on the accuracy or currency of information contained on this platform.</p>
                    
                    <p>You must take your own precautions to ensure that the process that you employ for accessing this platform does not expose you to the risk of viruses, malicious computer code, or other forms of interference which may damage your own computer system or device.  For the removal of doubt, we do not accept responsibility for any interference or damage to your own computer system or device which arises in connection with your use of this platform or any linked website or application.</p>
                    
                    <h3>Limitation of liability</h3>
                    
                    <p>We are not liable for any loss or damage, however caused (including, but not limited to, by our negligence) suffered by you in connection with this agreement or your use of this platform.</p>
                    
                    <p>If the Competition and Consumer Act 2010 (Cth) or any other legislation states that there is a guarantee in respect of goods or services supplied, and our liability for breach of that guarantee may not be excluded but may be limited, our liability for such breach is limited to, in the case of a supply of goods, replacing the goods or supplying equivalent goods or repairing the goods, or in the case of supply of services, supplying the services again or paying the cost of having the services supplied again.</p>
                    
                    <h3>Indemnity</h3>
                    
                    <p>You agree to indemnify us for all damages, losses, penalties, fines, expenses and costs (including legal costs) which arise out of or relate to your use of this platform, any information that you provide to us via this platform or any damage that you may cause to this platform.  This indemnification includes, without limitation, liability relating to copyright infringement, defamation, invasion of privacy, trade mark infringement and contraventions of the Competition and Consumer Act 2010 (Cth).</p>
                    
                    <h3>Access</h3>
                    
                    <p>Access to this platform may be withdrawn at any time without notice.  These terms and conditions will survive any such withdrawal.</p>
                    
                    <h3>Governing law and jurisdiction</h3>
                    
                    <p>If a dispute arises regarding these terms of use, the laws of Queensland, Australia, will apply.  In relation to any such dispute, you agree to submit to the non-exclusive jurisdiction of the courts of Queensland, Australia.</p>
                    
                    <p>If you access this platform in a jurisdiction other than Queensland, Australia, you are responsible for compliance with the laws of that jurisdiction, to the extent that they apply.</p>
                    
                    
				</div>					
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>