<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Course Details</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body class="pg-player">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
				<div class="summary-banner">
					<picture class="background" role="presentation">
						<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/max.jpg">
						<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/x-lrg.jpg">
						<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/lrg.jpg">
						<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/med.jpg">
						<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/sml.jpg">
						<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/min.jpg">
						<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/agrifoods/course-list-hero/max.jpg" alt="">
					</picture>
					<img class="logo" src="img/brand/agrifoods-logo.png" alt="AgriFoods Logo">
					<p class="course-collection">
						Digital Strategy 101
					</p>
					<h1 class="title">
						Developing A Digital Strategy
					</h1>
				</div>
				
				<div class="spacing-container video-and-utilities">
					<div class="preview-video-container fluid-video">
						<div class="wrapper">
							<iframe src="//www.youtube.com/embed/Bo_f8mV5khg?rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="side-utilities">
						<div class="simpletab-container">
							<a href="#tab-questions" class="blend-in simpletab-tab" data-behaviour="tab-button" data-selected="true">
								<img src="svg/icons/questions.svg">
								<span class="visuallyhidden">Questions</span>
							</a>
							<a href="#tab-notes" class="blend-in simpletab-tab" data-behaviour="tab-button">
								<img src="svg/icons/notes.svg">
								<span class="visuallyhidden">Notes</span>
							</a>
							<a href="#tab-modules" class="blend-in simpletab-tab" data-behaviour="tab-button">
								<img src="svg/icons/modules.svg">
								<span class="visuallyhidden">Modules</span>
							</a>
							<a href="#tab-resources" class="blend-in simpletab-tab" data-behaviour="tab-button">
								<img src="svg/icons/resources.svg">
								<span class="visuallyhidden">Resources</span>
							</a>
						</div>
						<div class="simpletab simpletab-target" id="tab-questions">
							<?php include 'partials/player-tab-questions.php'; ?>
						</div>
						<div class="simpletab simpletab-target" id="tab-notes">
							<?php include 'partials/player-tab-notes.php'; ?>
						</div>
						<div class="simpletab simpletab-target" id="tab-modules">
							<?php include 'partials/player-tab-modules.php'; ?>
						</div>
						<div class="simpletab simpletab-target" id="tab-resources">
							<?php include 'partials/player-tab-resources.php'; ?>
						</div>
					</div><?php /* .side-utilities */ ?>
				</div>

				<div class="spacing-container aux-tools">
					<div class="module-navigation">
						<a class="button filled-pc blend-in" href="">
							&lt;&lt; Back to Courses
						</a>
						<a class="button filled-pc blend-in" href="">
							&lt; Previous Module
						</a>
						<a class="button filled-pc blend-in" href="">
							Next Module &gt;
						</a>
					</div>

				</div>

			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>