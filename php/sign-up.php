<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Intstructor Profile</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body id="general-content-page" class="pg-instructor-profile">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
            	
                <picture class="background" role="presentation">
                    <img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif" alt="">
                    
                    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" valign="middle">
                        	<h1 class="status-info small-page-heading">
                                Sign Up
                            </h1>
                        </td>
                      </tr>
                    </table>
                </picture>

				<div class="certificate-section">
                    
                    <div class="sign-up-box-container">
                        <div class="sign-up-header">
                            <h3>JOIN THINK DIGITAL TODAY</h3>
                            And start learning the digital skills you need to grow your business today... for free                    
                        </div>
                        
                        <div class="sign-up-full sign-up-form float-left">
                            <form>
                                <div class="form-input-container">
                                    <input type="text" placeholder="Full Name" />
                                </div>
                                
                                <div class="form-input-container">
                                    <input type="text" placeholder="Email" />
                                </div>
                                
                                <div class="form-input-container">
                                    <input type="text" placeholder="Postcode" />
                                </div>
                                
                                <div class="form-input-container">
                                    <input type="password" placeholder="Password" />
                                </div>
                                
                                <div class="form-input-container">
                                    <select>
                                        <option value="" disabled selected>How did you find out about us?</option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                        <option value="3">Option 3</option>
                                        <option value="4">Option 4</option>
                                        <option value="5">Option 5</option>
                                    </select>
                                </div>
                                
                                <input type="submit" name="submit" value="Sign Up" id="sign-up-submit" />
                            </form>
                            
                            <div class="sign-up-disclaimer">By joining Think Digital you agree to our privacy policy and terms of use</div>
                        </div>
                        
                        <div class="clear"></div>
                    </div><!-- .sign-up-box-container -->
                    
				</div>					
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>