<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Think Digital Home</title>
		<?php require_once("partials/head-meta.php"); ?>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	</head>
	<body class="pg-brand-home">		        
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav class="with-buttons" data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="#" id="sign-up-button" class="button filled-pc">
						Sign Up
					</a>
					<a href="#" id="sign-in-button" class="button filled-sc">
						Sign In
					</a>
				</nav>
			</header>

			<main id="main" role="main"> 
				<div class="hero-banner">
					<a href="">
						<picture>
							<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/max.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/max@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/x-lrg.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/x-lrg@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/lrg.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/lrg@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/med.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/med@2x.jpg 2x">
							<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/sml.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/sml@2x.jpg 2x">
							<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/min.jpg 1x, <?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/min@2x.jpg 2x">
							<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage-hero/max.jpg" alt="">
						</picture>
					</a>
					<h1 class="visuallyhidden">
						Think Digital
					</h1>
				</div>

				<div class="two-part-feature pic-left">
					<img src="img/blank.gif" style="background-image:url(<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage/courses-galore.gif);" class="pic" role="presentation">
					<div class="content">
						<h3 class="title">
							Explore Courses Galore
						</h3>
						<div class="details">
							<h3 class="subtitle">
								Learn new digital skills in your own time online
							</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
							</p>
							<a class="button filled-pc" href="">
								START EXPLORING
							</a>
						</div>
					</div>
				</div>
				<div class="two-part-feature pic-right brand-primary-color">
					<img src="img/blank.gif" style="background-image: url(<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage/how-it-works.gif);" class="pic" role="presentation">
					<div class="content">
						<h3 class="title">
							How It Works
						</h3>
						<div class="details">
							<h3 class="subtitle">
								3 easy steps to learning digital strategy
							</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
							</p>
							<a class="button filled-pc" href="">
								SIGN UP NOW
							</a>
						</div>
					</div>
				</div>
				<div class="two-part-feature pic-left">
					<img src="img/blank.gif" style="background-image: url(<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/homepage/meet-tim.jpg);" class="pic" role="presentation">
					<div class="content">
						<h3 class="title">
							Access to expert knowledge
						</h3>
						<div class="details">
							<h3 class="subtitle">
								Digital insight with course instructor Tim Gentle
							</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
							</p>
							<a class="button filled-pc" href="">
								GET TO KNOW ME
							</a>
						</div>
					</div>
				</div>

				<h2 class="top-courses-title">
					Popular Think Digital Courses
				</h2>

				<div class="course-summary-list">
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
				</div> <?php /* course summary list */ ?>

				<div class="spacing-container bottom-cta">
	
					<h2 class="title">
						Start learning new digital skills today
					</h2>
					<p>
						Enhance your business through the use of digital tools
					</p>
					<a href="" class="button">
						START LEARNING
					</a>
				</div>
		
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>