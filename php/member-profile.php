<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Member Profile</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body>
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main" class="pg-member-profile">

				<div class="certificate-section">
					<picture class="background" role="presentation">
						<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif">
						<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/x-lrg.gif">
						<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/lrg.gif">
						<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/med.gif">
						<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/sml.gif">
						<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/min.gif">
						<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif" alt="">
					</picture>
					<h1 class="status-info">
						Member Profile
					</h1>
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/sample-avatar.png" alt="Your Avatar" class="avatar">
					<p class="name">
						John Artbuckle Smith
					</p>	
                    
                    <div id="edit-profile-form">
                    	<div class="profile-form-output output-success">Profile updated successfully.</div>
                        <div class="profile-form-output output-error">An error occured. Please try again.</div>
                    	<form>
                        	<input type="text" placeholder="Email" value="john.smith@thinkdigital.com.au" class="profile-input" />
                            <input type="password" placeholder="Password" class="profile-input" />
                            <input type="password" placeholder="Confirm password" class="profile-input" />
                            <input type="text" placeholder="First name" class="profile-input" />
                            <input type="text" placeholder="Last name" class="profile-input" />
                            <input type="submit" value="Edit profile" class="button filled-pc" />
                        </form>
                    </div>

				</div>				

			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>