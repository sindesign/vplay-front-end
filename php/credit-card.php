<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Member Profile</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body>
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main" class="pg-member-profile">

				<div class="certificate-section">
                    
                    <div id="edit-profile-form" class="credit-card-form">
                        <div class="profile-form-output output-error">An error occured. Please try again.</div>
                    	<form>
                        	<input type="text" placeholder="Card Number" class="profile-input" />
                            <input type="text" placeholder="Expiry Month (MM)" class="profile-input" />
                            <input type="text" placeholder="Expiry Year (YY)" class="profile-input" />
                            <input type="text" placeholder="CVC" class="profile-input" />
                            <input type="submit" value="Make Payment" class="button filled-pc" />
                        </form>
                    </div>

				</div>				

			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>