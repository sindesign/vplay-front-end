<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Intstructor Profile</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body id="general-content-page" class="pg-instructor-profile">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
            	
                <picture class="background" role="presentation">
                    <img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif" alt="">
                    
                    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" valign="middle">
                        	<h1 class="status-info small-page-heading">
                                Success!
                            </h1>
                        </td>
                      </tr>
                    </table>
                </picture>

				<div class="certificate-section">
                    
                    <div class="sign-up-box-container">
                       <div class="sign-up-half float-left">
                            <div class="sign-up-success-left">
                                <img src="../img/think-digital-logo-small.png" alt="Think Digital" />
                                
                                <h3>Thanks for joining Think Digital</h3>
                                
                                <p>Time to start learning the digital skills you need to grow your business.</p>
                                
                                <h4>Share the love</h4>
                                
                                <div class="share-the-love">
                                    <a href="#"><img src="../img/share-facebook.png" alt="Facebook" /></a>
                                    <a href="#"><img src="../img/share-twitter.png" alt="Twitter" /></a>
                                    <a href="#"><img src="../img/share-google.png" alt="Google+" /></a>
                                    <a href="#"><img src="../img/share-linkedin.png" alt="LinkedIn" /></a>
                                    <a href="#" style="margin-right:0;"><img src="../img/share-email.png" alt="Email" /></a>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="clear"></div>
                                
                                <a href="#" class="start-learning">Start Learning</a>
                            </div>
                        </div>
                        
                        <div class="sign-up-half sign-up-mascot float-right">
                            <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td align="right" valign="bottom"><img class="responsive-img" src="../img/mascot-thank-you.png" alt="Thanks for joining" />  </td>
                              </tr>
                            </table> 
                        </div>
                        
                        <div class="clear"></div>
                    </div><!-- .sign-up-box-container -->
                    
				</div>					
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>