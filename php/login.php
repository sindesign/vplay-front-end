<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Intstructor Profile</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body id="general-content-page" class="pg-instructor-profile">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">
            	
                <picture class="background" role="presentation">
                    <img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif" alt="">
                    
                    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" valign="middle">
                        	<h1 class="status-info small-page-heading">
                                Login
                            </h1>
                        </td>
                      </tr>
                    </table>
                </picture>

				<div class="certificate-section">
                    
                    <div class="sign-up-box-container">
                        
                        <div class="sign-up-full sign-up-form login float-left">
                            <form>                                
                                <div class="form-input-container">
                                    <input type="text" placeholder="Email" />
                                </div>
                                                                
                                <div class="form-input-container">
                                    <input type="password" placeholder="Password" />
                                </div>                                
                              
                                <div class="forgot-password"><a href="#">I've forgotten my password</a></div>                             
                                
                                <input type="submit" name="submit" value="Login" id="sign-up-submit" />
                            </form>
                        </div>
                        
                        <div class="clear"></div>
                    </div><!-- .sign-up-box-container -->
                    
				</div>					
			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>