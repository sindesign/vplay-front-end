<!DOCTYPE html>
<?php require_once("partials/vars.php"); ?>
<html lang="en" class="no-js">
	<head>
		<title>Congratulations!</title>
		<?php require_once("partials/head-meta.php"); ?>
	</head>
	<body class="pg-finished-course">
		<?php require_once("partials/browser-notice.php"); ?>
		<div id="body-wrap">
			<header>
				<?php require_once("partials/header.php"); ?>
				<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/td-logo.gif" alt="Think Digital logo" class="logo">
				<img src="img/menu.png" alt="Menu" class="menu-toggle">
				<nav data-state="closed">
					<a href="">
						Browse By Course
					</a>
					<a href="">
						Browse By Instructor
					</a>
					<a href="">
						My Courses
					</a>
					<a href="">
						Sign Out
					</a>
				</nav>
				<a href="" class="user-dropdown-link" data-state="closed">
					John Smith
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/no-avatar.jpg" alt="">
				</a>
			</header>

			<main id="main" role="main">

				<div class="certificate-section">
					<picture class="background" role="presentation">
						<source media="(min-width: <?php echo BP_MAX; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif">
						<source media="(min-width: <?php echo BP_X_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/x-lrg.gif">
						<source media="(min-width: <?php echo BP_LRG; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/lrg.gif">
						<source media="(min-width: <?php echo BP_MED; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/med.gif">
						<source media="(min-width: <?php echo BP_SML; ?>)" srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/sml.gif">
						<source srcset="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/min.gif">
						<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/finished-course/max.gif" alt="">
					</picture>
					<h1 class="status-info">
						Course Completed
					</h1>
					<img src="<?php echo BRAND_IMAGES_FOLDER; ?>/thinkdigital/sample-avatar.png" alt="Your avatar" class="avatar">
					<p class="name">
						John Artbuckle Smith
					</p>
					<p class="congratulations">
						Congratulations
					</p>
					<p class="results-summary">
						You have successfully completed the course 
						<span class="course-name">
							Developing a Digital Strategy
						</span>
					</p>
					<p class="">
						Feel free to write a review of the course and don't forget to download your certificate of completion below.
					</p>

					<div class="share-options">
						<h2 class="title">
							Tell your friends
						</h2>
						<ul class="social-links">
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via Facebook</span>
									<span class="socicon socicon-facebook"></span>
								</a>
							</li>
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via Twitter</span>
									<span class="socicon socicon-twitter"></span>
								</a>
							</li>
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via Google+</span>
									<span class="socicon socicon-google"></span>
								</a>
							</li>
							<li>
								<a href="" class="social-link">
									<span class="visuallyhidden">Share via LinkedIn</span>
									<span class="socicon socicon-linkedin"></span>
								</a>
							</li>
						</ul>
					</div>

					<div class="next-actions">
						<a href="" class="button filled-pc">
							Rate &amp; Review Course
						</a>
						<a href="" class="button filled-sc">
							Download Certificate
						</a>
						<a href="" class="button filled-pc">
							Refer Your Friends
						</a>
					</div>

				</div>

				<h2 class="brand-color-title">
					More Courses By Tim Gentle
				</h2>

				<div class="course-summary-list">
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
					<article class="course-summary-tile-small">
						<?php include("partials/course-summary-tile-small.php"); ?>			
					</article>
				</div>

				<p class="view-courses-container">
					<a href="" class="button filled-pc">
						View all courses
					</a>
				</p>

				<h2 class="more-courses-title brand-color-title">
					Recommended Courses
				</h2>

				<div class="course-summary-list">
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
					<article class="course-summary-tile tall">
						<?php include("partials/course-summary-tile.php"); ?>
					</article>
				</div> <?php /* course summary list */ ?>

			</main>

			<?php require_once("partials/footer.php"); ?>
			<?php require_once("partials/footer-scripts.php"); ?>
		</div>
	</body>
</html>