<h1>
	Think Digital / Agrifoods LMS Front-End Preview
</h1>

<ul>
	<li>
		<a href="think-digital-home.php">
			1. Think Digital Homepage
		</a>
	</li>
	<li>
		<a href="think-digital-home_logged-in.php">
			2. Think Digital Homepage (Logged In)
		</a>
	</li>
	<li>
		<a href="courses-list.php">
			3. Courses List
		</a>
	</li>
	<li>
		<a href="course-details.php">
			4. Course Details
		</a>
	</li>
	<li>
		<a href="player.php">
			5. Player Page
		</a>
	</li>
	<li>
		<a href="finished-course.php">
			6. Finished Course
		</a>
	</li>
	<li>
		<a href="member-profile.php">
			7. Member Profile
		</a>
	</li>
	<li>
		<a href="instructor-profile.php">
			8. Instructor Profile
		</a>
	</li>
    <li>
		<a href="terms.php">
			9. Terms &amp; Conditions
		</a>
	</li>
	<li>
		<a href="credit-card.php">
			10. Credit Card
		</a>
	</li>
</ul>