var buildDir = 'build/';

module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),

	//TASKS GO HERE
	watch: {
		sass: {
			files: ['scss/**', 'php/**', 'js/**', 'img/**', 'svg/**', 'fonts/**'],
			tasks: ['clean', 'sass', 'copy']
		}
	},
	clean: [buildDir],
	sass: {
		dist: {
			options: {
				style: 'compressed'
			},
			files: [
				{
					expand: true,
					flatten: true,
					src: ['scss/*.scss'],
					dest: buildDir + 'css/',
					ext: '.css'
				}
			]
		}
	},
	imagemin: {
		dist: {
			options: {
				optimizationLevel: 7,
				progressive: true,
				interlaced: true
			},
			files: [
				{
					expand: true,
					src: ['img/**/*.{png,jpg,gif}'],
					dest: buildDir
				}
			]
		}
	},
	copy: {
		main: {
			files: [
				{
					expand: true,
					src: ['fonts/**'],
					dest: buildDir
				},
				{
					expand: true,
					flatten: true,
					src: ['php/*'],
					dest: buildDir,
					filter: 'isFile'
				},
				{
					expand: true,
					flatten: true,
					src: ['php/*/**'],
					dest: buildDir + 'partials',
					filter: 'isFile'
				},
				{
					expand: true,
					flatten: true,
					src: ['css/**'],
					dest: buildDir
				},
				{
					expand: true,
					src: ['img/**'],
					dest: buildDir
				},
				{
					expand: true,
					src: ['js/**'],
					dest: buildDir
				},
				{
					expand: true,
					src: ['svg/**'],
					dest: buildDir
				}
			]
		}
	}
	});

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['clean', 'sass', 'imagemin', 'copy']);
	grunt.registerTask('serve', ['watch']);

};
