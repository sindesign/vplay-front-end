var app = app || {};

app.buildNotes = function() {
	
	var nc = app.notesContainer;
	var notes = app.notes;

	var buildString = '';

	if (!nc) {
		return;
	}

	for (var ii=0; ii<notes.length; ii++) {
		buildString += '<p class="note' + (ii === 0 ? ' note-hidden' : '') + '">';
		buildString += '<a href="javascript:void(0);" class="delete-note-button">';
		buildString +=		'Delete';
		buildString += '</a>';
		buildString += notes[ii].text;
		buildString += '</p>';
	}

	app.notesContainer.innerHTML = buildString;

	var deleteButtons = app.notesContainer.querySelectorAll('.delete-note-button');

	for (var jj=0; jj<deleteButtons.length; jj++) {
		deleteButtons[jj].addEventListener('click', app.deleteNote);
	}

	setTimeout(function() {
		var firstNote = app.notesContainer.querySelector('.note.note-hidden');

		firstNote.className = firstNote.className.replace('note-hidden','');
	}, 100);
};

app.getNotesString = function() {

	var notesString = '';

	notesString += '<h1>STORED NOTES:</h1>\n';

	for(var ii=0; ii<app.notes.length; ii++) {
		notesString += (ii+1) + ' - ' + app.notes[ii].text + '<br>\n';
	}

	return notesString;
};

app.downloadNotes = function() {

	var pageContent = 'data:text/html, ' + app.getNotesString();

	window.open(pageContent);
};

app.notesChanged = function() {
	//call AJAX here to update saved notes for this module
};

app.deleteNote = function(e) {
	var clickedButton = e.target;
	var parentNote = clickedButton.parentNode;

	var allNotes = app.notesContainer.querySelectorAll('.note');
	var deletedNoteIndex = -1;

	// find which note in the array it was based on where in the notes list it is
	for (var ii=0; ii<allNotes.length; ii++) {
		if (parentNote == allNotes[ii]) {
			deletedNoteIndex = ii;
		}
	}

	// delete that one
	if (deletedNoteIndex > -1) {
		app.notes.splice(deletedNoteIndex, 1);
	}

	// remove the DOM element
	parentNote.parentNode.removeChild(parentNote);

	app.notesChanged();
};

app.resetNotes = function() {
	app.buildNotes();
};

app.submitNote = function() {
	var noteText = app.noteSubmitForm.note.value;

	if (!noteText) {
		return;
	}

	var noteObj = {};

	noteObj.text = noteText;

	app.notes.unshift(noteObj);

	app.resetNotes();

	app.noteSubmitForm.note.placeholder = '';
	app.noteSubmitForm.note.value = '';
	app.noteSubmitForm.note.focus();
};

app.initNotes = function() {
	app.notesContainer = document.getElementById('notes-container');
	app.noteSubmitForm = document.getElementById('note-submit-form');

	if (app.notesContainer) {
		app.buildNotes();
	}

	if (app.noteSubmitForm) {
		app.noteSubmitForm.addEventListener('submit', app.submitNote);
	}
};