$(document).ready(init);

function init() {
	var tabsOnPage = $('#main').find('[data-behaviour="tab-button"]');

	if (tabsOnPage.length > 0) {
		tabsOnPage.simpleTabs();
	}

	if (app && app.initNotes) {
		app.initNotes();
	}

	$('header .menu-toggle').on('click',function() {
		var toggleItems = $('header nav').add('header .user-dropdown-link');

		if (toggleItems.first().attr('data-state') == "closed") {
			toggleItems.attr('data-state', 'open');
		} else {
			toggleItems.attr('data-state', 'closed');
		}
	});
}

function getQueryVariable(variable)
{
	var query = window.location.search.substring(1);
	var vars = query.split("&");

	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable){return pair[1];}
	}

	return(false);
}

/*
// Sign up box pop up commands
$("#sign-up-button").click(function(){
	$("#sign-up-box").css("display","block");
});

$(".pop-up-close-btn").click(function(){
	$("#sign-up-box").css("display","none");
});

// Sign up success box pop up commands
// This will need to be removed
$("#sign-in-button").click(function(){
	$("#sign-up-success-box").css("display","block");
});

$(".pop-up-close-btn").click(function(){
	$("#sign-up-success-box").css("display","none");
});
*/