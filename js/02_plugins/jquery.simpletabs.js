$.fn.simpleTabs = function() {
	
	var allTabs = this;

	function simpleTabs_tabClicked(e) {
		var clickedTab = $(e.target).closest('.simpletab-tab');

		for (var ii = 0; ii< allTabs.length; ii++) {
			var currentTabButton = $(allTabs[ii]);
			currentTabButton.attr('data-selected', false);
			var currentTabTarget = $(currentTabButton.attr('href')); // finds the #item that is stored in the href of the tab
			currentTabTarget.addClass('simpletab-hidden');
		}

		clickedTab.attr('data-selected', true);

		var clickedTabTarget = $(clickedTab.attr('href'));
		clickedTabTarget.removeClass('simpletab-hidden');

		e.preventDefault(); // avoid scrolling to element
	}

	function simpleTabs_clickActiveTab() {
		for (var ii = 0; ii< allTabs.length; ii++) {
			var currentTabButton = $(allTabs[ii]);
			if (currentTabButton.attr('data-selected') == "true") {
				currentTabButton.trigger('click');
			}
		}
	}

	for (var ii = 0; ii< allTabs.length; ii++) {
		var currentTabButton = $(allTabs[ii]);
		var currentTabTarget = $(currentTabButton.attr('href')); // finds the #item that is stored in the href of the tab

		currentTabButton.on('click', simpleTabs_tabClicked);
		currentTabTarget.addClass('simpletab simpletab-hidden');

		simpleTabs_clickActiveTab();
	}
};